import { createRouter, createWebHistory } from 'vue-router';
import LoginView from '../views/LoginView.vue';
import HomeView from '../views/HomeView.vue';
import TransactionView from '@/views/TransactionView.vue';
import TransacHistoryView from '@/views/TransacHistoryView.vue';
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      components: {
        default: LoginView
      },
      meta: {
        layout: 'FullLayout'
      }
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue')
    },
    {
      path: '/login',
      name: 'login',
      components: {
        default: LoginView
      },
      meta: {
        layout: 'FullLayout'
      }
    },
    {
      path: '/transaction',
      name: 'transaction',
      components: {
        default: TransactionView,
        menu: () => import('@/components/menus/MainMenu.vue'),
        header: () => import('@/components/headers/MainHeader.vue')
      },
      meta: {
        layout: 'MainLayout'
      }
    },
    {
      path: '/transactionHistory',
      name: 'transactionhistory',
      components: {
        default: TransacHistoryView,
        menu: () => import('@/components/menus/MainMenu.vue'),
        header: () => import('@/components/headers/MainHeader.vue')
      },
      meta: {
        layout: 'MainLayout'
      }
    }
  ]
});

export default router;
