import { ref, computed } from 'vue';
import { defineStore } from 'pinia';
import auth from '@/services/auth';
import { useMessageStore } from './message';
import { useLoadingStore } from './loading';
import router from '@/router';
import { useUserStore } from './users';

export const useAuthStore = defineStore('auth', () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const userStore = useUserStore();
  const authName = ref('');

  const isLogin = () => {
    const user = localStorage.getItem('user');
    if (user) {
      return true;
    }
    return false;
  };
  const login = async (username: string, password: string): Promise<void> => {
    loadingStore.isLoading = true;
    try {
      const res = await auth.login(username, password);
      // console.log('enter login');
      // console.log(res.data);
      // console.log(res.data.user);
      localStorage.setItem('user', JSON.stringify(res.data.user));
      
      console.log("after login");
      
      console.log(localStorage.getItem('user'));
      
      localStorage.setItem('token', res.data.access_token);
      router.push('/transaction');
    } catch (e) {
      messageStore.showError('Username หรือ Password ไม่ถูกต้อง');
    }
    loadingStore.isLoading = false;
  };
  const logout = () => {
    // authName.value = "";
    localStorage.removeItem('user');
    localStorage.removeItem('token');
    router.replace('/');
  };

  return { login, logout, isLogin };
});
