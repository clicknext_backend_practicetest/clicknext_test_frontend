import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading';
import transactionService from '@/services/transaction';
import type Transaction from '@/types/Transaction';
import { useMessageStore } from './message';
import { useUserStore } from './users';

export const useTransactionStore = defineStore('transactions', () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const userStore = useUserStore();
  const transferTransactions = ref<Transaction[]>([]);
  const receiveTransactions = ref<Transaction[]>([]);
  // const storedUserString = localStorage.getItem('user');
  const editedTransaction = ref({Action:'DEPOSIT',Amount:0, sender:'',receiver:''})
  // let userId = '';
  // if (storedUserString) {
  //   const user = JSON.parse(storedUserString);
  //   userId = user._id;
  //   // console.log(user);
  // };

  async function getTransactions() {
    loadingStore.isLoading = true;
    try {
      const res = await transactionService.getTransferTransactions(userStore.user._id!);// get transfer
      transferTransactions.value = res.data;
      const res2 = await transactionService.getReceiveTransactions(userStore.user._id!);
      receiveTransactions.value =res2.data;
    } catch (e) {
      console.log(e);
      messageStore.showError('ไม่สามารถดึงข้อมูล transaction ได้');
    }
    loadingStore.isLoading = false;
  }

  async function saveTransaction(){
    console.log("userStore.user._id here:", userStore.user._id);
    
    editedTransaction.value.sender = userStore.user._id!;
    loadingStore.isLoading = true;
    try {
        const res = await transactionService.saveTransaction(editedTransaction.value);
      // console.log(editedTransaction);
      await getTransactions();
      messageStore.showInfo('ทำรายการสำเร็จ');
    } catch (e) {
      messageStore.showInfo(e.response.data.message);
      console.log(e);
    }
    loadingStore.isLoading = false;
    userStore.getUserBalance();

  }
  async function resetEditedUser() {
    await delay(100);
    editedTransaction.value = {Action:'DEPOSIT',Amount:0, sender:'',receiver:''}
  }
  function delay(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

  return { getTransactions,transferTransactions, receiveTransactions,saveTransaction,editedTransaction,resetEditedUser }
})
