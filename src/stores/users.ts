import { ref } from 'vue';
import { defineStore } from 'pinia';
import type User from '@/types/Users';
import userService from '@/services/user';
import { useMessageStore } from './message';
import { useLoadingStore } from './loading';
export const useUserStore = defineStore('User', () => {
  const messageStore = useMessageStore();
  const loadingStore = useLoadingStore();
  const dialog = ref(false);
  const users = ref<User[]>([]);
  const user = ref<User>({balance:0,name:'',user_name:'',_id:''});
  const userBalance = ref(0);
  // const storedUserString = localStorage.getItem('user');
  const userId = ref('');
  function getLocalUser(){
  const storedUserString = localStorage.getItem('user');
     if (storedUserString) {
    user.value = JSON.parse(storedUserString);
    userId.value = user.value._id!;
    // console.log(user);
  };
  }
 
  // const editedUser = ref<User>({ user_name: '', email: '', password: '' });

  async function getUsers() {
    loadingStore.isLoading = true;
    try {
      const res = await userService.getUsers();
      users.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError('ไม่สามารถดึงข้อมูล User ได้');
    }
    loadingStore.isLoading = false;
  }
  async function getUserBalance() {
    console.log('enter bal');
    
    loadingStore.isLoading = true;
    try {
      console.log('userId:',user.value._id);
      
      const res = await userService.getById(user.value._id!);
      user.value = res.data;
      console.log("in getBal res",res.data);
      console.log("in getBal user",user.value);
      
      userBalance.value = res.data.balance;
      console.log(userBalance.value);
      
    } catch (e) {
      console.log(e);
      messageStore.showError('ไม่สามารถดึงข้อมูล User ได้');
    }
    loadingStore.isLoading = false;
  }

  // async function saveUser() {
  //   loadingStore.isLoading = true;
  //   try {
  //     if (editedUser.value.id) {
  //       const res = await userService.updateUser(editedUser.value.id, editedUser.value);
  //     } else {
  //       const res = await userService.saveUser(editedUser.value);
  //     }

  //     dialog.value = false;
  //     await getUsers();
  //   } catch (e) {
  //     messageStore.showError('ไม่สามารถบันทึก User ได้');
  //     console.log(e);
  //   }
  //   resetEditedUser();
  //   loadingStore.isLoading = false;
  // }
  // async function saveUserWithoutLogin() {
  //   loadingStore.isLoading = true;
  //   try {
  //     if (editedUser.value.id) {
  //       const res = await userService.updateUser(editedUser.value.id, editedUser.value);
  //     } else {
  //       const res = await userService.saveUser(editedUser.value);
  //     }

  //     dialog.value = false;
  //   } catch (e) {
  //     messageStore.showError('ไม่สามารถบันทึก User ได้');
  //     console.log(e);
  //   }
  //   resetEditedUser();
  //   loadingStore.isLoading = false;
  // }

  // function editUser(user: User) {
  //   editedUser.value = JSON.parse(JSON.stringify(user));
  //   dialog.value = true;
  // }

  async function deleteUser(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await userService.deleteUser(id);
      await getUsers();
    } catch (e) {
      console.log(e);
      messageStore.showError('ไม่สามารถลบ User ได้');
    }
    loadingStore.isLoading = false;
  }

  // async function resetEditedUser() {
  //   await delay(100);
  //   editedUser.value = {
  //     user_name: '',
  //     email: '',
  //     password: ''
  //   };
  // }
  function delay(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  return { users, getUsers, dialog, deleteUser,userBalance,getUserBalance,user,getLocalUser };
});
