import type User from "./Users";

export default interface Transaction {
  _id?: number;
  Current?: number;
  Action: string;
  sender: User; // User ID
  receiver?: User; // User ID
  Amount: number;
  createdAt: Date;
}
