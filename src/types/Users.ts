export default interface User {
  _id?: string;
  user_name: string;
  name: string;
  password?: string;
  balance: number;
  createdAt?: Date;
  updatedAt?: Date;

}
