import type User from '@/types/Users';
import http from './axios';
function getUsers() {
  return http.get('/users');
}
function getById(id: string) {
  return http.get(`/users/${id}`);
}

function saveUser(user: User) {
  return http.post('/users', user);
}

function updateUser(id: number, user: User) {
  return http.patch(`/users/${id}`, user);
}

function deleteUser(id: number) {
  return http.delete(`/users/${id}`);
}

export default { getUsers, saveUser, updateUser, deleteUser,getById };
