import http from './axios'
import type Transaction from '@/types/Transaction';

function getTransferTransactions(id: string) {
    return http.get(`transactions/sender/${id}`,);
}
function getReceiveTransactions(id: string) {
    return http.get(`transactions/receiver/${id}`,);
}
function saveTransaction(Transaction: {Action: string, sender: string, receiver: string, Amount: number}) {
    return http.post("/transactions", Transaction);
  }

export default { getTransferTransactions,getReceiveTransactions , saveTransaction }